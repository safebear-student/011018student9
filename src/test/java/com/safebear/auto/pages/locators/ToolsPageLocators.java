package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    // messages

    private By failedLoginMessage = By.xpath(".//*[@class='container']/p/b");

    //input fields

    private By searchField = By.xpath(".//input[contains(@placeholder,\"Type the Name\")]");

    //buttons

    private By searchButton = By.xpath(".//button[@class=\"btn btn-info\"]");

    //text

    private By seleniumTool = By.xpath(".//td[.='Selenium']");
}
