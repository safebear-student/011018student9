package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;

public class Utils {

    private static final String URL = System.getProperty("url","http://toolslist.safebear.co.uk:8080/");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver(){

        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");

        // This is the location of my geckodriver (for Firefox browser)
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        ChromeOptions options = new ChromeOptions();


        switch (BROWSER){

            case "chrome":
                options.addArguments("window-size=1366,768");
                return new ChromeDriver(options);

            case "chrome_headless":
                options.addArguments("headless","disable-gpu");
                return  new ChromeDriver(options);

            case "firefox":
                return new FirefoxDriver();

            default:

                return new ChromeDriver();

        }



    }


}
