Feature: Login

  In order to access the website
  As a user
  I want to know if my login is successful

  Rules/Acceptance Criteria:
  * The User must be informed if the login information is incorrect
  * The User must be informed if the login is successful

  Glossary:
  * User: Someone who wants to create Tools List using our application.
  * Supporters: This is what the customer calls 'Admin' user.

  Scenario Outline: A user attempts to log in to an application
    Given I navigate to the login page
    When I enter the login details for a '<userType>'
    Then I can see the following message: '<validationMessage>'
    Examples:
      | userType    | validationMessage                 |
      | validUser   | Login Successful                  |
      | invalidUser | Username or Password is incorrect |
