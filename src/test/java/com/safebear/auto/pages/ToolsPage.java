package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {
    ToolsPageLocators toolsPageLocators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage(){
        return driver.findElement(toolsPageLocators.getFailedLoginMessage()).getText();
    }


    // Entering text into the search field
    public void enterTextIntoSearchField(String toolName){
        driver.findElement(toolsPageLocators.getSearchField()).sendKeys(toolName);
    }

    // Clicking on the search button
    public void clickOnSearchButton(){
        driver.findElement(toolsPageLocators.getSearchButton()).click();
    }

    // Checking that the selenium tool has been returned / checking the selenium tool exists
    public boolean checkForSeleniumTool(){
        return driver.findElement(toolsPageLocators.getSeleniumTool()).isDisplayed();
    }


}
