Feature: Search

  Rules:
  * When you search for a tool that exists then it is returned
  * You must be logged in in order to search for a tool

  Background: Ensure that I am logged in
    Given that I am logged in

  @new
  Scenario: A user searches for a tool
    Given the Selenium tool exists
    When I search for the Selenium tool
    Then the Selenium tool is returned