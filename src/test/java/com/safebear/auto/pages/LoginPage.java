package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators loginPageLocators = new LoginPageLocators();
    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void enterUsername(String username) {
        driver.findElement(loginPageLocators.getUsernameLocator()).sendKeys(username);
    }

    public void enterPassword(String password){
        driver.findElement(loginPageLocators.getPasswordLocator()).sendKeys(password);
    }

    public void clickLoginButton(){
        driver.findElement(loginPageLocators.getLoginButtonLocator()).click();
    }

    public String checkForFailedLoginWarning(){
        return driver.findElement(loginPageLocators.getFailedLoginMessage()).getText();
    }

}